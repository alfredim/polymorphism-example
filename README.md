IKEA STORE

When creating the store specify the number of store objects;

Create an interface IkeaObject with a method getDescription() which should return a String
 
Create Food abstract class with a method getServed() which should return a String with the value “Get the food from the restaurant and pay.”

Create Furniture abstract class with a method purchaseFurniture() which should return a String with the value “Get furniture from warehouse and pay.”

All provided objects should implement IkeaObject interface

All Food Objects should extend Food class

All Furniture Objects should extend Furniture class

Create a Buildable interface with method that returns Boolean isBuildable

Chair, Table and Lamp should implement Buildable interface (Char and table should return true, and lamp return false)

Store open() logic:
Print to console 	every IkeaObject description from getDescription()

If object is instance of Food print to console getServed() String
Print to console “Eating <food item name>” from eat() 
Example: “Eating meat balls”

If object is instance of Furniture print to console purchaseFurniture()

If object is instance of Buildable check isBuildable
If isBuildable print to console “Assembling the furniture before using it” 
If is not Buildable print to console “Using new IKEA furniture” hardcoded

If is not buildable means it’s a fabric, so just print to console something like
Getting comfortable 
