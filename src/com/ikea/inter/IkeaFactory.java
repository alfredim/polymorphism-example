package com.ikea.inter;

import com.ikea.inter.exception.IkeaException;
import com.ikea.inter.food.FishAndChips;
import com.ikea.inter.food.MeatBalls;
import com.ikea.inter.food.Salad;
import com.ikea.inter.furniture.Chair;
import com.ikea.inter.furniture.Lamp;
import com.ikea.inter.furniture.Table;
import com.ikea.inter.furniture.fabrics.Blanket;
import com.ikea.inter.furniture.fabrics.Pillow;

import static java.lang.Math.random;

/**
 * This class will randomly create Ikea Objects
 */
public class IkeaFactory {

    public static Object[] createObjects(int arraySize) throws IkeaException {

        //TODO change the generic java Object to an abstract Ikea object with the method getDescription()
        Object[] ikeaObjects = new Object[arraySize];

        for (int i = 0; i < arraySize; i++) {
            int odd = (int) (random() * 8);

            switch (odd) {
                case 0:
                    ikeaObjects[i] = new FishAndChips();
                    break;
                case 1:
                    ikeaObjects[i] = new MeatBalls();
                    break;
                case 2:
                    ikeaObjects[i] = new Salad();
                    break;
                case 3:
                    ikeaObjects[i] = new Chair();
                    break;
                case 4:
                    ikeaObjects[i] = new Lamp();
                    break;
                case 5:
                    ikeaObjects[i] = new Table();
                    break;
                case 6:
                    ikeaObjects[i] = new Blanket();
                    break;
                case 7:
                    ikeaObjects[i] = new Pillow();
                    break;
                default:
                    throw new IkeaException("Value of bounds: " + odd);
            }
        }
        return ikeaObjects;
    }
}
