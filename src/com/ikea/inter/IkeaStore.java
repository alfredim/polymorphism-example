package com.ikea.inter;

import com.ikea.inter.exception.IkeaException;

import static com.ikea.inter.IkeaFactory.createObjects;
import static java.lang.System.*;
import static java.lang.System.err;

public class IkeaStore {

    Object[] ikeaItems;

    public IkeaStore(int numberOfObjects) {
        try {
            ikeaItems = createObjects(numberOfObjects);
        } catch (IkeaException e) {
            err.println("Unable to create objects: " + e.getMessage());
        }
    }

    public void open() {

        out.println("Preparing for opening!");
    }
}
