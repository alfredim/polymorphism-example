package com.ikea.inter.exception;

public class IkeaException extends Exception {
    public IkeaException(String errorMessage) {
        super(errorMessage);
    }
}
